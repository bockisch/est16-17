package kfzwerkstatt;

import java.util.GregorianCalendar;

import kfzwerkstatt.Verkaeufer.Prioritaet;

public class Mechaniker {

	private final String mName;
	private final String mVorname;
	private final String mId;

	public int sumArbeit;

	private int lohnProZeiteinheit = 15;

	public Mechaniker(String vorname, String name, String id) {
		mId = id;
		mName = name;
		mVorname = vorname;
	}

	public void bearbeiteJob(Kunde kunde, Prioritaet prio,
			String fehlerbeschreibung) {

		System.out.println("Beginne mit Arbeit!");
		// je nach Prio wird die Arbeit potentiell schneller erleidgt :-D
		int dauer = 100;
		if (prio == Verkaeufer.Prioritaet.DRINGEND)
			dauer = 50;
		else if (prio == Verkaeufer.Prioritaet.WICHTIG)
			dauer = 80;
		else if (prio == Verkaeufer.Prioritaet.NORMAL)
			dauer = 100;

		System.out.println("Arbeit beendet! Dauer " + dauer);
		// Millisekunden werden hier exemplarisch als Minuten gez�hlt

		// je nach Prio wird ein Aufschlag erhoben pro Zeiteinheit
		int kostensatz = lohnProZeiteinheit;
		if (prio == Verkaeufer.Prioritaet.DRINGEND)
			kostensatz += 10;
		else if (prio == Verkaeufer.Prioritaet.WICHTIG)
			dauer += 50;

		int lohn = dauer * kostensatz;
		sumArbeit += lohn;
		kunde.addBezahlt(lohn);
	}

	public int gibArbeitskostenZureuck() {
		return sumArbeit;
	}

	public String getMName() {
		return mName;
	}

	public String getMVorname() {
		return mVorname;
	}

	public String getMId() {
		return mId;
	}

	/**
	 * @return the lohnProZeiteinheit
	 */
	public int getLohnProZeiteinheit() {
		return lohnProZeiteinheit;
	}

	/**
	 * @param lohnProZeiteinheit
	 *            the lohnProZeiteinheit to set
	 */
	public void setLohnProZeiteinheit(int lohnProZeiteinheit) {
		this.lohnProZeiteinheit = lohnProZeiteinheit;
	}

}
