package kfzwerkstatt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Werkstatt {

	private List mitarbeiter;
	private List kunden;

	public Werkstatt() {
		mitarbeiter = new ArrayList();
		kunden = new ArrayList();
	}

	public void addMitarbeiterVerkaeufer(Verkaeufer v) {
		mitarbeiter.add(v);
	}

	public void addMitarbeiterMechaniker(Mechaniker m) {
		mitarbeiter.add(m);
	}

	public void addKunde(Kunde k) {
		kunden.add(k);
	}

	public int gibNettoEinnahmen() {
		int summe = 0;

		for (Object o : mitarbeiter) {
			if (o instanceof Verkaeufer) {
				summe += (((Verkaeufer) o).sumVerkaeufe) * 0.81; // 19% Mwst
				// abgezogen
			} else if (o instanceof Mechaniker) {
				summe += (((Mechaniker) o).gibArbeitskostenZureuck()) * 0.81;
			}
		}// for

		return summe;
	}

	public int gibBruttoEinnahmen() {
		int summe = 0;

		for (Object o : mitarbeiter) {
			if (o instanceof Verkaeufer) {
				summe += (((Verkaeufer) o).sumVerkaeufe);
			} else if (o instanceof Mechaniker) {
				summe += (((Mechaniker) o).gibArbeitskostenZureuck());
			}
		}// for
		return summe;
	}

	public Map<String, Integer> gibKundenbetraege() {
		Map<String, Integer> resultat = new HashMap<String, Integer>();
		for (Object o : kunden) {
			Kunde k = (Kunde) o;
			resultat.put(k.getVorname() + " " + k.getName(), k.getBezahlt()); 
		}
		return resultat;
	}
}
