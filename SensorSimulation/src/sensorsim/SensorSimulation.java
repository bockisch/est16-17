package sensorsim;
import java.awt.MouseInfo;
import java.awt.Toolkit;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.TimeUnit;

/**
 * Sensor simulation. Provides values of type DOUBLE, range [0..1], depending on mouse y position up to 33 times per second.
 * @author Felix Rieger
 *
 */
public class SensorSimulation extends Observable {
	
	private volatile boolean loopIsRunning = true;	/** is set to false when the simulation loop should be stopped **/
	private Thread t;	/** Simulation thread **/
	
	/**
	 * Starts the sensor simulation. Simulation runs in its own thread
	 */
	private void startSimulation() {
		Runnable r = new Runnable() {
			
			@Override
			public void run() {
				double yMax = Toolkit.getDefaultToolkit().getScreenSize().getHeight();	// get screen height for scaling
				while(loopIsRunning) {
					try {
						TimeUnit.MILLISECONDS.sleep(30);	// wait for 30ms between acquisitions
					} catch (InterruptedException e) {
					}
					int mouseY = MouseInfo.getPointerInfo().getLocation().y;	// get mouse pointer y coordinate
					setChanged();
					notifyObservers((mouseY - 0.5 * yMax) / (0.5 * yMax));	// scale y value to [-1..1] and notify observers
				}
			}
		};

		t = new Thread(r);
		t.setDaemon(true);
		t.start();
	}
	
	@Override
	public synchronized void addObserver(Observer o) {
		if (countObservers() == 0) {
			startSimulation();
		}
		super.addObserver(o);
	}
	
	/**
	 * Sensor simulation
	 * Provides double values, range [-1..1], corresponding to mouse y position, up to 34 times per second.
	 */
	public SensorSimulation() {
		
	}
	
	public void stop() {
		loopIsRunning = false;
	}
	
}
